@echo off

cd C:
cd %USERPROFILE%\AppData\Local\chia-blockchain\app-1.1.6\resources\app.asar.unpacked\daemon

echo Stopping all services if any started

chia stop all -d

echo starting harvester

start chia start harvester -r

echo Harvester succesfully started
echo Press any key to continue
pause > nul
